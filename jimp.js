const jimp = require('jimp');

async function main() {
    let fonte = await jimp.loadFont(jimp.FONT_SANS_32_BLACK)
    let mask = await jimp.read('mascara.png')
    //let avatar = await jimp.read('avatar.jpg')
    let bg = await jimp.read('fundo.png')
    jimp.read("https://image.freepik.com/vector-gratis/perfil-avatar-hombre-icono-redondo_24640-14044.jpg")
        .then(
            avatar => {
                avatar.resize(130, 130)
                mask.resize(130, 130)
                avatar.mask(mask)
                bg.print(fonte, 170, 175, "Valesko")
                bg.composite(avatar, 30, 70).write('foto.png')
            }
        )
        .catch(err => { console.log(err) });
}

main()