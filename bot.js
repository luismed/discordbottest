const Discord = require('discord.js');
const jimp = require('jimp')
const client = new Discord.Client();
const config = require('./config.json');

client.on("ready", () => {
    console.log(`Bot iniciado, com ${client.users.size} usuarios`);
    client.user.setGame(`Eu estou em ${client.guilds.size} servidores`);
});

client.on('guildMemberAdd', async member => {

    let canal = client.channels.get("589494225171906599")
    let fonte = await jimp.loadFont(jimp.FONT_SANS_32_BLACK)
    let mask = await jimp.read('mascara.png')
    //let avatar = await jimp.read('avatar.jpg')
    let bg = await jimp.read('fundo.png')
    jimp.read(member.user.avatarURL)
        .then(
            avatar => {
                avatar.resize(130, 130)
                mask.resize(130, 130)
                avatar.mask(mask)
                bg.print(fonte, 170, 175, member.user.username)
                bg.composite(avatar, 30, 70).write('foto.png')
                canal.send(``, { files: ["foto.png"]})
            }
        )
        .catch(err => { console.log(err) });
});

client.on("message", async message => {
    if (message.author.bot) return;
    if (message.channel.type === "dm") return;

    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const comando = args.shift().toLowerCase();
    
    if (comando === "ping") {
        const m = await message.channel.send("Ping?");
        let number = 3;
        let  random = Math.floor(Math.random() * (number - 1 + 1)) + 1 ;
        switch(random){
            case 1: m.edit(`Truco! A latência é ${m.createdTimestamp - message.createdTimestamp}ms. A Latencia da API é ${Math.round(client.ping)}ms`); break;
            case 2: m.edit(`Bingo! A latência é ${m.createdTimestamp - message.createdTimestamp}ms. A Latencia da API é ${Math.round(client.ping)}ms`); break;
            case 3: m.edit(`Uno! A latência é ${m.createdTimestamp - message.createdTimestamp}ms. A Latencia da API é ${Math.round(client.ping)}ms`); break;
            
        }
    }
})

client.login(config.token);